from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import TemplateView,LoginView,LogoutView
from django.core.exceptions import ImproperlyConfigured
from django.utils.encoding import force_text
from django.views.generic import RedirectView, CreateView, FormView
from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from .forms import *

# Create your views here.
class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = 'login.html'
    success_url = reverse_lazy('index_paciente')
    def form_valid(self, form):
        url = ""
        print('formValid')
        login(self.request,form.get_user())
        if self.request.user.es_paciente:
            url = 'index_paciente'
        if self.request.user.es_medico:
            url = 'index_medico'
        return HttpResponseRedirect(url)
class ElegirCuentaView(TemplateView):
    template_name = 'elegir_cuenta.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['panel']='Crear Usuario'
        return context

class Prueba(TemplateView):
    template_name = 'prueba.html'

class UserMedicoCreateView(CreateView):
    model = Medico
    form_class = MedicoUserForm
    template_name = 'alta_medico.html'

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('index_medico')

class UserPacienteCreateView(CreateView):
    model = Paciente
    form_class = PacienteUserForm
    template_name = 'alta_paciente.html'
    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('index_paciente')

class IndexPaciente(LoginRequiredMixin,TemplateView):
    template_name ='index_paciente.html'


class IndexMedico(LoginRequiredMixin,TemplateView):
    template_name ='index_medico.html'

