from django import forms
from .models import Paciente, Medico, Usuario
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.contrib.auth.models import User, Group

class MedicoUserForm(UserCreationForm):
    password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Contraseña'}))
    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Repetir contraseña'}))

    especialidad = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control col-md-7 col-xs-12',
        'required': 'required',
        'autofocus': False,
        'placeholder': 'especialidad'
    }))
    matricula = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control col-md-7 col-xs-12',
        'required': 'required',
        'autofocus': False,
        'placeholder': 'matricula'
    }))


    class Meta:
        model = Usuario
        fields = [
            'first_name',
            'last_name',
            'username',
            'password1',
            'password2'
        ]
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido'}),
            'username': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre de usuario'}),
            'direccion': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre de usuario'})

        }

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.es_medico = True
        user.save()
        medico = Medico.objects.create(usuario=user)
        user.groups.add(Group.objects.get(name="Es_Medico"))
        matricula= self.cleaned_data['matricula']
        especialidad= self.cleaned_data['especialidad']
        medico.matricula=matricula
        medico.especialidad=especialidad
        medico.save()
        return user

class PacienteUserForm(UserCreationForm):
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Contraseña'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Repetir contraseña'}))
    direccion = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control col-md-7 col-xs-12',
        'required': 'required',
        'autofocus': False,
        'placeholder': 'Direccion'
    }))
    telefono = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control col-md-7 col-xs-12',
        'required': 'required',
        'autofocus': False,
        'placeholder': 'Telefono'
    }))
    fecha_de_nacimiento = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control col-md-7 col-xs-12',
        'required': 'required',
        'autofocus': False,
        'placeholder': 'Fecha De Nacimiento'
    }))
    obra_social = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control col-md-7 col-xs-12',
        'required': 'required',
        'autofocus': False,
        'placeholder': 'Obra Social'
    }))
    edad = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control col-md-7 col-xs-12',
        'required': 'required',
        'autofocus': False,
        'placeholder': 'Edad'
    }))
    dni = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control col-md-7 col-xs-12',
        'required': 'required',
        'autofocus': False,
        'placeholder': 'Dni'
    }))

    class Meta:
        model = Usuario
        fields = [
            'first_name',
            'last_name',
            'username',
            'password1',
            'password2'
        ]
        widgets = {
            'first_name': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nombre'}),
            'last_name': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Apellido'}),
            'username': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nombre de usuario'})
        }

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.es_paciente = True
        user.save()
        paciente = Paciente.objects.create(usuario=user)
        user.groups.add(Group.objects.get(name="Es_Paciente"))
        direccion= self.cleaned_data['direccion']
        telefono = self.cleaned_data['telefono']
        fecha_de_nacimiento = self.cleaned_data['fecha_de_nacimiento']
        obra_social=self.cleaned_data['obra_social']
        edad=self.cleaned_data['edad']
        dni=self.cleaned_data['dni']
        paciente.direccion=direccion
        paciente.telefono=telefono
        paciente.fecha_de_nacimiento=fecha_de_nacimiento
        paciente.obra_social=obra_social
        paciente.edad=edad
        paciente.dni=dni
        paciente.save()
        return user


