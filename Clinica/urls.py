"""Clinica URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from administracion.views import *
from django.contrib.auth.decorators import login_required
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',LoginFormView.as_view(), name='login'),
    path('logout/',LogoutView.as_view(next_page='login'),name='logout'),
    path('elegir_cuenta/', ElegirCuentaView.as_view(), name='elegir_cuenta'),
    path('prueba/', Prueba.as_view(), name='prueba'),
    path('alta_medico/', UserMedicoCreateView.as_view(), name='alta_medico'),
    path('alta_paciente/',UserPacienteCreateView.as_view(), name='alta_paciente'),
    path('index_paciente/',IndexPaciente.as_view(),name='index_paciente'),
    path('index_medico/',IndexMedico.as_view(),name='index_medico'),

]
